package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		// TODO: Implement method
		if (index < 0 || index >= n)
			throw new IndexOutOfBoundsException("Index is out of bounds");
		return (T) elements[index];
	}
	
	@Override
	public void add(int index, T element) {
		// TODO: Implement method
		if (index < 0 || index > n)
			throw new IndexOutOfBoundsException("Index is out of bounds");
			
		if (elements.length < n+2)
				ensureCapacity();
			
		while (index < size()) {
			T tempElem = get(index);
			elements[index] = element;
			element = tempElem;
			index++;
		}
		elements[index] = element;
		n++;
	}
	
	public void ensureCapacity() {
		int newCapacity = elements.length * 2;
	elements = Arrays.copyOf(elements, newCapacity);
	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}